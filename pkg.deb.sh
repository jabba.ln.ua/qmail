#!/bin/sh

###
PKG=qmail-fade
VER=1.03
BRANCH=u104.fade

####
HDIR=/home/joker/.mta
VCSDIR=/mnt/vcs/git/dev/net/qmail/src

###
CWD=`pwd`
SRC=${CWD}/src

#CARE: $HDIR/qold is a place for patched source (from origin, mean debian, ububtu, etc.)

#### UPDATE & WORK WITH VCS
update_vcs()
{
  ##svn commit -m "DEB auto update" $HDIR/qmail && svn update $HDIR/qmail
	cd ${VCSDIR}/..
  git fetch && git pull origin
  git checkout ${BRANCH}

}

commit_vcs()
{

  git commit -am "some fix" && git push origin

}
####
update() {

  update_vcs

}

#### MAKE SOME PATCH
make_patch() {

  rm -f ${HDIR}/qnew/*
  cp -v ${VCSDIR}/* ${HDIR}/qnew

  diff -Naurp ${HDIR}/qold ${HDIR}/qnew >${VCSDIR}/patches/qmail-fade.patch
  #exit 0

}

#####
patch() {

  make_patch

}

#### MAKE DEB PACKAGE
make_deb() {

  cp -R mta $PKG-$VER
  cd $PKG-$VER

  #find . -type d -name '.svn' -print0 | xargs -0 /bin/rm -R;
  dpkg-buildpackage -rfakeroot -S -sa
  echo "!!!!!!!!!!!!!!!      NEXT STEP    !!!!!!!!!!!!!!!!!!!!"
  dpkg-buildpackage -rfakeroot
  #dpkg -i ../$PKG*.deb

}

#### PURGE OLD MTA
purge_old_mta() {

  #purge exim4
  dpkg --force-depends --purge exim4-base exim4 exim4-config exim4-daemon-light exim4-daemon-heavy exim4-daemon-custom

  #
  #dpkg --force-depends --purge qmail-fade qmail-fade-src
}

####
purge() {

  purge_old_mta

}

#### SETUP ALL
install_deb() {

  cd ..
  rm -R $PKG-$VER

  sudo dpkg -i qm*.deb || exit 1
  sudo rm -R /tmp/qmail-fade

  sudo build-qmail-fade || exit 1
  sudo rm -R /tmp/qmail-fade

  cd ${CWD}

}

#### JUST... DO IT
setup() {

 update
 make
 install

}

####
case "$1" in

    'purge')
        purge
        ;;

    'patch')
        patch
        ;;

    'update')
        update
        ;;

    'make')
        make
        ;;


    'setup')
        setup
        ;;

    *)
        echo "usage $0 update|make|setup|patch|purge"

esac
